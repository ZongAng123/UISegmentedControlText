//
//  ZAUISegmentedControl.h
//  UISegmentedControlText
//
//  Created by 纵昂 on 2022/4/25.
//

#import <UIKit/UIKit.h>
#import "UISegmentedButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZAUISegmentedControl : UISegmentedControl
-(void)setupButtonBadge:(NSInteger)index number:(uint)number;

@end

NS_ASSUME_NONNULL_END
