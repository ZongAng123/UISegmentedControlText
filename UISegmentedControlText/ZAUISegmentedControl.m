//
//  ZAUISegmentedControl.m
//  UISegmentedControlText
//
//  Created by 纵昂 on 2022/4/25.
//

#import "ZAUISegmentedControl.h"

@interface ZAUISegmentedControl ()
{
    UISegmentedButton *button1;
    UISegmentedButton *button2;
    UISegmentedButton *button3;
    NSArray *myItems ;
}
@end

@implementation ZAUISegmentedControl

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self){
        button1 = [UISegmentedButton buttonWithType:UIButtonTypeCustom];
        button2 = [UISegmentedButton buttonWithType:UIButtonTypeCustom];
        button3 = [UISegmentedButton buttonWithType:UIButtonTypeCustom];
        
        [button1 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button2 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button3 addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button1 setHidden:true];
        [button2 setHidden:true];
        [button3 setHidden:true];
        
        [self addSubview:button1];
        [self addSubview:button2];
        [self addSubview:button3];
    }
    return self;
}

-(instancetype)initWithItems:(NSArray *)items
{
    self = [super initWithItems:items];
    if(self) {
        myItems = items;
    }
    return self;
}

-(void)setupButtonBadge:(NSInteger)index number:(uint)number
{
    
    if (index > [self numberOfSegments]-1) {
        return;
    }
    if(index == 0) {
        if(number == 0) {
            [button1 setHidden:true];
            [self setTitle:[myItems objectAtIndex:0] forSegmentAtIndex:0];
        } else {
            [self setTitle:[[myItems objectAtIndex:0] stringByAppendingString:@"　　"] forSegmentAtIndex:0];
            [button1 setHidden:false];
            button1.msgNum.text = [NSString stringWithFormat:@"%d",number<99 ? number : 99];
        }
    }
    if(index == 1) {
        if(number == 0) {
            [button2 setHidden:true];
            [self setTitle:[myItems objectAtIndex:1] forSegmentAtIndex:1];
        } else {
            [self setTitle:[[myItems objectAtIndex:1] stringByAppendingString:@"　　"] forSegmentAtIndex:1];
            [button2 setHidden:false];
            button2.msgNum.text = [NSString stringWithFormat:@"%d",number<99 ? number : 99];
        }
    }
    if(index == 2) {
        if(number == 0) {
            [button3 setHidden:true];
            [self setTitle:[myItems objectAtIndex:2] forSegmentAtIndex:2];
        } else {
            [self setTitle:[[myItems objectAtIndex:2] stringByAppendingString:@"　　"] forSegmentAtIndex:2];
            [button3 setHidden:false];
            button3.msgNum.text = [NSString stringWithFormat:@"%d",number<99 ? number : 99];
        }
    }
    
}



-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    if(myItems != nil && [myItems count] > 0) {
        if([myItems count] == 1) {
            [button1 setFrame:CGRectMake(0, 0,frame.size.width,frame.size.height)];

        } else if([myItems count] == 2) {
            [button1 setFrame:CGRectMake(0, 0,frame.size.width/2,frame.size.height)];
            [button2 setFrame:CGRectMake(frame.size.width/2, 0,frame.size.width/2,frame.size.height)];
            //[button3 removeFromSuperview];
        } else if([myItems count] == 3) {
            [button1 setFrame:CGRectMake(0, 0,frame.size.width/3,frame.size.height)];
            [button2 setFrame:CGRectMake(frame.size.width/3, 0,frame.size.width/3,frame.size.height)];
            [button3 setFrame:CGRectMake(2*frame.size.width/3, 0,frame.size.width/3,frame.size.height)];
        } else {
            [NSException raise:@"UISegmentedControlBadge exception." format:@"UISegmentedControlBadge only support maximum 3 segments now" ];
        }
    }
}

-(void)buttonClicked:(id)sender
{
    [button1 setHighlighted:false];
    [button2 setHighlighted:false];
    [button3 setHighlighted:false];
    if(sender == button1) {
        [self setSelectedSegmentIndex:0];
    }
    if(sender == button2) {
        [self setSelectedSegmentIndex:1];
    }
    if(sender == button3) {
        [self setSelectedSegmentIndex:2];
    }
    [self sendActionsForControlEvents:UIControlEventValueChanged];
    
}


-(void)layoutSubviews{
    [super layoutSubviews];
    [self bringSubviewToFront:button1];
    [self bringSubviewToFront:button2];
    [self bringSubviewToFront:button3];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
