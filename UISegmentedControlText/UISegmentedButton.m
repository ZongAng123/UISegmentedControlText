//
//  UISegmentedButton.m
//  UISegmentedControlText
//
//  Created by 纵昂 on 2022/4/25.
//

#import "UISegmentedButton.h"

@implementation UISegmentedButton


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self) {
        self.msgNotifybg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_tab_notif_bg"]];
        _msgNotifybg.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_msgNotifybg];
        
        self.msgNum = [[UILabel alloc] init];
        _msgNum.textAlignment = NSTextAlignmentCenter;
        [_msgNum setTextColor:[UIColor whiteColor]];
        _msgNum.font = [UIFont systemFontOfSize:13.0f];
        _msgNum.translatesAutoresizingMaskIntoConstraints = NO;
        [_msgNotifybg addSubview:_msgNum];
        
        [self updateConstraintsIfNeeded];
    }
    return self;
}


-(void)updateConstraints
{
    
    
    [super updateConstraints];
    NSDictionary *layoutDict = NSDictionaryOfVariableBindings(_msgNotifybg, _msgNum);

    NSLayoutConstraint* cn;
    
    cn = [NSLayoutConstraint constraintWithItem:_msgNotifybg
                                      attribute:NSLayoutAttributeTrailing
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self
                                      attribute:NSLayoutAttributeTrailing
                                     multiplier:1
                                       constant:-5];
    [self addConstraint:cn];
    cn = [NSLayoutConstraint constraintWithItem:_msgNotifybg
                                      attribute:NSLayoutAttributeCenterY
                                      relatedBy:NSLayoutRelationEqual
                                         toItem:self
                                      attribute:NSLayoutAttributeCenterY
                                     multiplier:1
                                       constant:0];
    [self addConstraint:cn];
    
    [_msgNotifybg addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_msgNum]|" options:0 metrics:nil views:layoutDict]];
    [_msgNotifybg addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_msgNum]|" options:0 metrics:nil views:layoutDict]];
    
    
    _msgNotifybg.layer.cornerRadius = 0;
    _msgNotifybg.clipsToBounds = YES;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
