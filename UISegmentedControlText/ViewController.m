//
//  ViewController.m
//  UISegmentedControlText
//
//  Created by 纵昂 on 2022/4/25.
//  主要代码
//

#import "ViewController.h"
#import "HHHViewController.h" //测试页面01
#import "KKKViewController.h" //测试页面02

// 屏幕高度
#define SCREEN_HEIGHT [[UIScreen mainScreen] bounds].size.height
// 屏幕宽度
#define SCREEN_WIDTH  [[UIScreen mainScreen] bounds].size.width


@interface ViewController ()
{
    NSMutableDictionary *listDic;
}
@property (nonatomic, strong) HHHViewController *noExamineVC; //售货柜订单
@property (nonatomic, strong) KKKViewController *readExamineVC;  //商城订单


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self initUISegment];
    
}

#pragma mark - UISegmentedControl 初始化布局 2022-4-25 zongang
-(void)initUISegment{
    //先生成存放标题的数据
    NSArray *array = [NSArray arrayWithObjects:@"售货柜订单",@"商城订单", nil];
    //初始化UISegmentedControl
    UISegmentedControl *segment = [[UISegmentedControl alloc]initWithItems:array];
    segment.frame = CGRectMake(40, 60, SCREEN_WIDTH, 30);
//    根据内容定分段宽度
    segment.apportionsSegmentWidthsByContent = YES;
//    开始时默认选中下标(第一个下标默认是0)
    segment.selectedSegmentIndex = 0;
//    控件渲染色(也就是外观字体颜色)
//    segment.tintColor = [UIColor whiteColor];
//  设置指定索引选项的宽度(设置下标为2的分段宽度)
    [segment setWidth:170.0 forSegmentAtIndex:0];
    [segment setWidth:170.0 forSegmentAtIndex:1];
    //添加事件
    [segment addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];

//    添加到视图
//    self.navigationItem.titleView = segment;
    [self.view addSubview:segment];
    // 默认显示OneVc的内容
    [self.view addSubview:self.noExamineVC.view];
    
    
}

//点击不同分段就会有不同的事件进行相应
-(void)change:(UISegmentedControl *)sender{
    NSUInteger segIndex = [sender selectedSegmentIndex];
    UIViewController *controller = [self controllerForSegIndex:segIndex];
    NSArray *array2 = [self.view subviews];
    NSLog(@"array2-->%@",array2);
    //将当旧VC的view移除，然后在添加新VC的view
    if (array2.count != 0) {
        if (segIndex == 0) {
            [_readExamineVC.view removeFromSuperview];
            NSLog(@"remove--oneVC");
        }else if (segIndex == 1){
            [_noExamineVC.view removeFromSuperview];
            NSLog(@"remove--twoVC");
        }
    }
    [self.view addSubview:controller.view];
    
}

//根据字典中是否存在相关页面对应的key，没有的话存储
- (UIViewController *)controllerForSegIndex:(NSUInteger)segIndex {
    NSString *keyName = [NSString stringWithFormat:@"VC_%ld",segIndex];
    
    UIViewController *controller = (UIViewController *)[listDic objectForKey:keyName];
    
    if (!controller) {
        if (segIndex == 0) {//售货柜订单
            controller = self.noExamineVC;
            
        }else if (segIndex == 1) {//商城订单
            controller = self.readExamineVC;
        }
        [listDic setObject:controller forKey:keyName];
    }
    
    return controller;
}

//初始化左边的
- (HHHViewController *)noExamineVC {
    if (_noExamineVC == nil) {
        _noExamineVC = [[HHHViewController alloc] init];
        _noExamineVC.view.frame = CGRectMake(0, 100, SCREEN_WIDTH, SCREEN_HEIGHT);
        
    }
    return _noExamineVC;
}

//初始化右边的
- (KKKViewController *)readExamineVC {
    if (_readExamineVC == nil) {
        _readExamineVC = [[KKKViewController alloc] init];
        _readExamineVC.view.frame = CGRectMake(0, 100, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    return _readExamineVC;
}






@end
