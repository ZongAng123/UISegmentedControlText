//
//  BeiFeiViewController.m
//  UISegmentedControlText
//
//  Created by 纵昂 on 2022/4/25.
//

#import "BeiFeiViewController.h"
#import "ZAUISegmentedControl.h" //引入
#import "HHHViewController.h" //测试页面01
#import "KKKViewController.h" //测试页面02

@interface BeiFeiViewController ()
@property (nonatomic, strong)ZAUISegmentedControl *segmentedControl;
@property (assign, nonatomic) int count; //int值
@property (nonatomic, strong)HHHViewController * jjjjV;
@property (nonatomic, strong)KKKViewController * kkkkkV;
@property (nonatomic, strong) UIView *firstview;
@property (nonatomic, strong) UIView *secondview;

@end

@implementation BeiFeiViewController
-(UIView *)firstview {
    if (_firstview == nil) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(0, 140, self.view.bounds.size.width, self.view.bounds.size.height);
        view.backgroundColor = [UIColor redColor];
        _firstview = view;
        
        [self.view addSubview:_firstview];
    }
    return _firstview;
}

-(UIView *)secondview{
    if (_secondview == nil) {
        UIView *view = [[UIView alloc] init];
        view.frame = CGRectMake(0, 140, self.view.bounds.size.width, self.view.bounds.size.height );
        view.backgroundColor = [UIColor blueColor];
        _secondview = view;
        
        [self.view addSubview:_secondview];
    }
    return _secondview;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.count = 0; //默认0
    
    [self initUISegment];
    
}

#pragma mark - UISegmentedControl 初始化布局 2022-4-25 zongang
-(void)initUISegment{
#pragma mark - 分段控制
    self.segmentedControl = [[ZAUISegmentedControl alloc] initWithItems:[[NSArray alloc]initWithObjects:@"售货柜订单", @"商城订单", nil]];
    self.segmentedControl.frame = CGRectMake(100, 100, 200, 30);
    [self.view addSubview:self.segmentedControl];
    self.segmentedControl.selectedSegmentIndex = 0;
    [self.segmentedControl addTarget:self action:@selector(segmentedHandler:) forControlEvents:UIControlEventValueChanged];
    
}

#pragma mark - 跳转页面 2022-4-25  跳转页面有问题
- (void)segmentedHandler:(id)sender{
    self.count++;
    [self.segmentedControl setupButtonBadge:self.segmentedControl.selectedSegmentIndex number:self.count];

    switch (_segmentedControl.selectedSegmentIndex) {
        case 0:{
            NSLog(@"点击了第一按钮");
            [_secondview didMoveToWindow];
            [self firstview];
        }
            break;
        case 1:{
            NSLog(@"点击了第二按钮");
            [self.firstview didMoveToWindow];
            [self secondview];
        }
            break;
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
