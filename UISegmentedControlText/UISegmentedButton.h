//
//  UISegmentedButton.h
//  UISegmentedControlText
//
//  Created by 纵昂 on 2022/4/25.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UISegmentedButton : UIButton
@property (nonatomic, strong) UIImageView *msgNotifybg;
@property (nonatomic, strong) UILabel *msgNum;

@end

NS_ASSUME_NONNULL_END
